#ifndef PROG2_3_HPP_
#define PROG2_3_HPP_

#include <vector>
#include <string>

using namespace std;

class Parser {
public:
	Parser();
	virtual ~Parser();
	bool Parse(vector<string> tokens);
};

#endif