#include "prog2_3.hpp"

Parser::Parser() {
}

Parser::~Parser() {
}

bool is_single_token(string token) {
	if (token == "pop" || token == "add" || token == "sub" || token == "mul"
			|| token == "div" || token == "mod" || token == "skip") {
		return true;
	}
	return false;
}

bool is_double_token(string token) {
	if (token == "push" || token == "save" || token == "get") {
		return true;
	}
	return false;
}

bool Parser::Parse(vector<string> tokens) {
	if(tokens.size()==1){
		if(!is_single_token(tokens.at(0))){
			return false;
		}
	}else{
		if(!is_double_token(tokens.at(0))){
			return false;
		}
	}
	return true;
}
