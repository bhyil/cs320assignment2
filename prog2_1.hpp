#ifndef PROG2_1_HPP_
#define PROG2_1_HPP_

#include <vector>
#include <string>
#include <exception>

using namespace std;

class Tokenizer {
public:
	Tokenizer();
	virtual ~Tokenizer();
	void Tokenize(string line);
	vector<string> GetTokens();
private:
	int index;
	vector< vector<string> >* tokens;
};

#endif