#include "prog2_1.hpp"

#include <iostream>
#include <string>

Tokenizer::Tokenizer() {
	index = 0;
	tokens = new vector<vector<string> >();
}

Tokenizer::~Tokenizer() {
	delete tokens;
}

bool is_int(string value) {
	for (size_t i = 0; i < value.size(); i++) {
		if (value[i] < '0' || value[i] > '9') {
			return false;
		}
	}
	return value.size() > 0;
}

bool is_token(string token) {
	if (token == "push" || token == "pop" || token == "add" || token == "sub"
			|| token == "mul" || token == "div" || token == "mod"
			|| token == "skip" || token == "save" || token == "get") {
		return true;
	}
	if(is_int(token)){
		return true;
	}
	return false;
}

void Tokenizer::Tokenize(string line) {
	vector<string> token;
	size_t pos = line.find(" ");
	while (pos != string::npos) {
		string value=line.substr(0,pos);
		if (!is_token(value)) {
			throw "Unexpected token: " + value;
		}
		token.push_back(value);
		line=line.substr(pos+1);
		pos = line.find(" ");
	}
	if (!is_token(line)) {
		throw "Unexpected token: " + line;
	}
	token.push_back(line);
	tokens->push_back(token);
}

vector<string> Tokenizer::GetTokens() {
	if (tokens->size() == 0) {
		throw string("No tokens");
	}
	vector<string> token = tokens->at(index);
	index = (index + 1) % tokens->size();
	return token;
}