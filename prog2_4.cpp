#include <iostream>
#include <fstream>
#include "prog2_1.hpp"
#include "prog2_3.hpp"

using namespace std;

int main(int argc, char* argv[]) {
	cout << "Assignment #2-4,Haibo Zhou,1050029976@qq.com" << endl;
	fstream file(argv[1]);
	Tokenizer tokenizer;
	Parser parser;
	int lineNum = 0;
	try {
		string line;
		while (getline(file, line)) {
			lineNum++;
			tokenizer.Tokenize(line);
		}
		for (int i = 0; i < lineNum; i++) {
			vector<string> token = tokenizer.GetTokens();
			if(!parser.Parse(token)){
				cout << "Parse error on line " << i+1 << ": ";
				for(size_t i=0;i<token.size();i++){
					if (token.size() == 1) {
						cout << " ";
					}
					cout << token.at(i);
				}
				cout << endl;
				return 0;
			}
		}
		for (int i = 0; i < lineNum; i++) {
			vector<string> token = tokenizer.GetTokens();
			for(size_t i=0;i<token.size();i++){
				if (i>0) {
					cout << ",";
				}
				cout << token.at(i);
			}
			cout << endl;
		}
	} catch (string &msg) {
		cout << "Error on line " << lineNum << ": " << msg << endl;
	}
	return 0;
}